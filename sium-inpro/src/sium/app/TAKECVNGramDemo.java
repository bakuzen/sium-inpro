package sium.app;

import inpro.incremental.PushBuffer;
import inpro.incremental.sink.FrameAwarePushBuffer;
import inpro.incremental.source.SphinxASR;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import sium.module.ResolutionModuleWAC;
import sium.nlu.context.Context;
import sium.nlu.language.LingEvidence;
import sium.nlu.stat.Distribution;
import wac.util.TakeRawSqlUtils;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4ComponentList;

public class TAKECVNGramDemo {
	
	static Logger log = Logger.getLogger(PentoRMRSDemo.class.getName());
	
	@S4Component(type = SphinxASR.class)
	public final static String PROP_ASR_HYP = "currentASRHypothesis";

	@S4ComponentList(type = PushBuffer.class)
	public final static String PROP_HYP_CHANGE_LISTENERS = SphinxASR.PROP_HYP_CHANGE_LISTENERS;
	

	int currentFrame = 0;
	List<PushBuffer> hypListeners = null;

	List<EditMessage<IU>> edits = new ArrayList<EditMessage<IU>>();
	private TakeRawSqlUtils take;

	private int max = 1000;
	private int numFolds = 10;
	private int foldSize = 100;
	private int numIters = 3;
	
	public TAKECVNGramDemo() {
		
	}
	

	private void run() throws SQLException, InterruptedException {
		
		double correct = 0.0;
		double total = 0.0;

		
		take = new TakeRawSqlUtils();
		take.createConnection(null);
		ArrayList<String> episodes = take.getAllEpisodes();
//		Collections.shuffle(episodes);
		
		for (int i=1; i<=3; i++) {
			
			System.out.println("Processing fold " + i + " out of " + numFolds);
			
			for (int iters=0; iters<numIters; iters++) {
				int j = 1;
				
				ConfigurationManager cm = new ConfigurationManager("src/sium/config/config.xml");
				PropertySheet ps = cm.getPropertySheet(PROP_ASR_HYP);
				hypListeners = ps.getComponentList(PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
				ResolutionModuleWAC resolution =  (ResolutionModuleWAC) cm.lookup("disc");
				
				resolution.toggleTrainMode();
				
	//			setup the training data
				for (String episode : episodes) {
					if (exceedsMax(j)) break;
				
					if (isTrainData(j, i)) {
						
						setContext(resolution, episode);
						
						ArrayList<LingEvidence> ling = take.getLingEvidence(episode);
						setGoldSlots(resolution, episode);
						processLine(ling);
					}
					j++;
				}
				
				resolution.train();
				resolution.toggleNormalMode();
				
				
	//			now, run evaluation on this fold
				j = 0;
				for (String episode : episodes) {
					if (exceedsMax(j)) break;
				
					if (!isTrainData(j, i)) {
						
						setContext(resolution, episode);
						
						ArrayList<LingEvidence> ling = take.getLingEvidence(episode);
						setGoldSlots(resolution, episode);
						processLine(ling);
						
	//					Now, evaluate....
						String gold = take.getGoldPiece(episode);
						Distribution<String> grounder = resolution.getGrounder().getPosterior();
						if (grounder != null && !grounder.isEmpty()) {
							if (grounder.getArgMax().getEntity().equals(gold))
								correct++;
						}
						
						total++;
					}
					j++;
				}
				
				resolution.clear();

			}
			System.out.println("Accuracy: " + (correct / total) + "(" +correct+ "/" + total +")");
		}
		take.closeConnection();
	}
	
	private void setGoldSlots(ResolutionModuleWAC resolution, String episode) throws SQLException {
		String gold = take.getGoldPiece(episode);
		resolution.setReferent(gold);
	}

	private void setContext(ResolutionModuleWAC resolution, String episode) throws SQLException {
		Context<String,String> context = take.getRawFeatures(episode);
		resolution.setContext(context);
	}

	private void processLine(ArrayList<LingEvidence> ling) {
		
		ArrayList<WordIU> ius = new ArrayList<WordIU>();
		
		WordIU prev = WordIU.FIRST_WORD_IU;
		
		for (LingEvidence ev : ling) {
			String word = ev.getValue("w1");
			if (word.equals("<s>")) continue;
			WordIU wiu = new WordIU(word, prev, null);
			ius.add(wiu);
			edits.add(new EditMessage<IU>(EditType.ADD, wiu));
			notifyListeners(hypListeners);
			prev = wiu;
		}
		
		for (WordIU iu : ius) {
			edits.add(new EditMessage<IU>(EditType.COMMIT, iu));
		}
		notifyListeners(hypListeners);
		
	}

	private boolean isTrainData(int j, int i) {
		return (j < (i-1) * foldSize) || (j > i * foldSize);
	}


	private boolean exceedsMax(int j) {
		return j > max;
	}

	public void notifyListeners(List<PushBuffer> listeners) {
		if (edits != null && !edits.isEmpty()) {
			//logger.debug("notifying about" + edits);
			currentFrame += 100;
			for (PushBuffer listener : listeners) {
				if (listener instanceof FrameAwarePushBuffer) {
					((FrameAwarePushBuffer) listener).setCurrentFrame(currentFrame);
				}
				// notify
				listener.hypChange(null, edits);
				
			}
			edits.clear();
		}
	}		
	
	public static void main(String[] args) {
		
		try {
			new TAKECVNGramDemo().run();
		}
		catch (SQLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
