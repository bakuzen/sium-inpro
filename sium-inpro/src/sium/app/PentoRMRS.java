package sium.app;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sium.module.PredicateLogicModule;
import sium.system.util.PentoSqlUtils;
import sium.system.util.TakeSqlUtils;
import inpro.incremental.PushBuffer;
import inpro.incremental.processor.Tagger;
import inpro.incremental.sink.FrameAwarePushBuffer;
import inpro.incremental.source.SphinxASR;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4ComponentList;

public class PentoRMRS {

	
	@S4Component(type = SphinxASR.class)
	public final static String PROP_ASR_HYP = "currentASRHypothesis";

	@S4ComponentList(type = PushBuffer.class)
	public final static String PROP_HYP_CHANGE_LISTENERS = SphinxASR.PROP_HYP_CHANGE_LISTENERS;

	ConfigurationManager cm;
	int currentFrame = 0;
	List<PushBuffer> hypListeners = null;
	PropertySheet ps;
	FileWriter writer;
	
	List<EditMessage<IU>> edits = new ArrayList<EditMessage<IU>>();
	
	public static void main(String[] args) {
		try {
			new PentoRMRS().run();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private void run() throws SQLException, IOException {
		writer  = new FileWriter(new File("/home/casey/Desktop/pento_rmrs.txt"));
		cm = new ConfigurationManager("src/sium/config/config.xml");
		ps = cm.getPropertySheet(PROP_ASR_HYP);
		hypListeners = ps.getComponentList(PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
		PentoSqlUtils pento = new PentoSqlUtils();
		pento.createConnection();
		
		ResultSet utts = pento.getAllUtterances();
		while (utts.next()) {
			String utt = utts.getString("utt").toLowerCase();
			String id = utts.getString("episode_id");
			System.out.println(id + " " + utt);
			processLine(utt);
			write(id, utt);
		}
		writer.close();
		pento.closeConnection();
	}
	
	private void write(String id, String utt) throws IOException {
		try {
			String rmrs = PredicateLogicModule.topFormula.toStringMultiLine();
			String syntax = PredicateLogicModule.topCA.toFullString();
			writer.write("<<< (" + id + ") " + utt +"\n");
			writer.write("lemma:" + Tagger.lemmas +"\n");
			writer.write("syntax:" +  syntax +"\n");
			writer.write(rmrs + "\n");
			writer.write(">>>\n");
		
		} 
		catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	private void processLine(String ling) {
		
		ArrayList<WordIU> ius = new ArrayList<WordIU>();
		
		WordIU prev = WordIU.FIRST_WORD_IU;
		
		String[] split = ling.split("\\s+");
		
		for (int i=0; i<split.length; i++) {
			String word = split[i];
			WordIU wiu = new WordIU(word, prev, null);
			ius.add(wiu);
			edits.add(new EditMessage<IU>(EditType.ADD, wiu));
			notifyListeners(hypListeners);
			prev = wiu;
		}
		
		for (WordIU iu : ius) {
			edits.add(new EditMessage<IU>(EditType.COMMIT, iu));
		}
		notifyListeners(hypListeners);
		
	}
	
	
	public void notifyListeners(List<PushBuffer> listeners) {
		if (edits != null && !edits.isEmpty()) {
			//logger.debug("notifying about" + edits);
			currentFrame += 100;
			for (PushBuffer listener : listeners) {
				if (listener instanceof FrameAwarePushBuffer) {
					((FrameAwarePushBuffer) listener).setCurrentFrame(currentFrame);
				}
				// notify
				listener.hypChange(null, edits);
				
			}
			edits = new ArrayList<EditMessage<IU>>();
		}
	}
	
	
	
}
