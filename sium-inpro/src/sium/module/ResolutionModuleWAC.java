package sium.module;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import wac.rr.grounding.DiscGrounder;
import sium.nlu.context.Properties;
import sium.nlu.context.Property;
import sium.nlu.stat.Distribution;
import sium.util.IUUtils;
import wac.rr.prob.LogisticRegression;
import wac.util.EvaluationUtils;
import wac.util.RandomUtils;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Boolean;
import edu.cmu.sphinx.util.props.S4Integer;
import edu.cmu.sphinx.util.props.S4String;
import inpro.incremental.unit.IU;

public class ResolutionModuleWAC extends ResolutionModuleNgram {
	
	
	@S4Integer(defaultValue = 1)
	public final static String NUM_NEGS = "numNegs";
	private int numNegs;
	
	@S4Boolean(defaultValue = false)
	public final static String LOAD_MODELS = "loadModels";
	private boolean loadModels;
	
	@S4String(defaultValue = "")
	public final static String MODELS_PATH = "modelsPath";
	
	HashMap<String,LogisticRegression> classifiers;
	
    HashMap<String,List<Properties<Property<String>>>> data = new HashMap<String,List<Properties<Property<String>>>>();
    HashMap<String,List<Integer>> target = new HashMap<String,List<Integer>>();
	
	/*
	 * (non-Javadoc)
	 * @see sium.module.ResolutionModuleNgram#newProperties(edu.cmu.sphinx.util.props.PropertySheet)
	 * 
	 * This basically does the same thing as ResoutionModuleNgram, except it doesn't need a Mapping class to 
	 * map between words and properties; this, instead, goes directly from properties to low-level features. 
	 */
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		numNegs = ps.getInt(NUM_NEGS);
		grounder = new DiscGrounder<String, String>();
		classifiers = new HashMap<String,LogisticRegression>();
		loadModels = ps.getBoolean(LOAD_MODELS);

		try {
			if (loadModels)
				loadModels(ps.getString(MODELS_PATH));
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		mapping = null;
	}
	
	@Override 
	protected void applyInstance(IU iu) {
		checkIsFirst(IUUtils.getWordIU(iu));
		String word = getLingEvidence(iu).getValue("w1");
		Distribution<String> inc = EvaluationUtils.evaluateEpisodeIncrement(classifiers, context, word);
		grounder.groundIncrement(context, inc);
	}
	
	@Override
	public void addTrainingInstance(IU iu) {
		
		String word = getLingEvidence(iu).getValue("w1");
		Properties<Property<String>> pos = context.getPropertiesForEntity(getReferent());
		
		if (!data.containsKey(word)) {
			data.put(word, new ArrayList<Properties<Property<String>>>());
			target.put(word, new ArrayList<Integer>());
		}
		data.get(word).add(pos);
		target.get(word).add(0);
		
		for (int i=0; i<numNegs; i++) {
			String nonGold = RandomUtils.chooseRandomNotGold(context, getReferent());
			Properties<Property<String>> neg = context.getPropertiesForEntity(nonGold);
			data.get(word).add(neg);
			target.get(word).add(1);
		}
	}
	
	@Override
	public void train() {
	    for (String word : data.keySet()) {
	    	LogisticRegression lr = new LogisticRegression();
		    	lr.trainSet(target.get(word), data.get(word));
	    	classifiers.put(word,  lr);
	    }
	}
	
	public void saveModels(String path) throws IOException {
		for (String word : classifiers.keySet()) {
			classifiers.get(word).saveModel(path + "/" + word + ".wac");
		}
	}
	
	@Override
	public void clear() {
		data.clear();
		target.clear();
		classifiers.clear();
		grounder.clear();
	}

	public void loadModels(String path) throws IOException {
		 for (final File fileEntry : new File(path).listFiles()) {
			 if (fileEntry.isDirectory()) continue;
			 if (!fileEntry.getName().endsWith(".wac")) continue;
			 String word = fileEntry.getName().replace(".wac", "");
			 LogisticRegression lr = new LogisticRegression();
			 lr.loadModel(path + fileEntry.getName());
			 classifiers.put(word, lr);
		 }
	}


}
